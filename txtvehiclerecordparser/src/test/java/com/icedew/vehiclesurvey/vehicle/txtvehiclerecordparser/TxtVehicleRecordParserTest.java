/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 *  - Wen Guan, <wen.guan@cern.ch>, 2015
 */

package com.icedew.vehiclesurvey.vehicle.txtvehiclerecordparser;

import static org.junit.Assert.assertEquals;

import com.icedew.vehiclesurvey.vehicle.txtvehiclerecordparser.TxtVehicleRecordParser;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.VehicleRecordService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class TxtVehicleRecordParserTest {

    private VehicleRecordManager vehicleRecordManager = null;
    private VehicleRecordService service = null;
    private TxtVehicleRecordParser vehicleRecordParser = null;

    /**
     * setup.
     */
    @Before
    public void setUp() {
        service = VehicleRecordService.getInstance();
        vehicleRecordManager = service.getVehicleRecordManager();
        vehicleRecordParser = new TxtVehicleRecordParser(vehicleRecordManager);
    }

    /**
     * read from string reader test.
     */ 
    @Test
    public void readFromStringTest() throws Exception {
        vehicleRecordManager.clear();
        String str = "A123322\n"
            + "A123422\n";
        StringReader reader = new StringReader(str);
        vehicleRecordParser.configure(reader);
        vehicleRecordParser.read();
        List<VehicleRecord> records = vehicleRecordManager.getVehicleRecords();
        assertEquals(records.size(), 1);
    }

    /**
     * read from file test.
     */ 
    @Test
    public void readFromFileTest() throws Exception {
        vehicleRecordManager.clear();
        System.setProperty("TxtVehicleRecordParser.inputResourceFile", "testinput.txt");
        vehicleRecordParser.configure();
        vehicleRecordParser.read();
        List<VehicleRecord> records = vehicleRecordManager.getVehicleRecords();
        assertEquals(records.size(), 1);
    }

    /**
     * read from bigger file test.
     */
    @Test
    public void readFromBiggerFileTest() throws Exception {
        vehicleRecordManager.clear();
        System.setProperty("TxtVehicleRecordParser.inputResourceFile", "testmoreinput.txt");
        vehicleRecordParser.configure();
        vehicleRecordParser.read();
        List<VehicleRecord> records = vehicleRecordManager.getVehicleRecords();
        assertEquals(records.size(), 11);
    }
}

