/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 * TxtVehicleRecordParser class.
 */


package com.icedew.vehiclesurvey.vehicle.txtvehiclerecordparser;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TxtVehicleRecordParser extends VehicleRecordParser {
    private static final Logger logger =
        Logger.getLogger(TxtVehicleRecordParser.class.getName());

    private Queue<Mark> parseQueue = new LinkedList<Mark>();
    private long day = 1;
    private long lastTime = -1;
    private long lastSouthHit = 0;
    private long lastNorthHit = 0;

    public TxtVehicleRecordParser() {
        super();
        logger.setLevel(Level.FINE);
    }

    public TxtVehicleRecordParser(VehicleRecordManager vehicleRecordManager) {
        super(vehicleRecordManager);
        logger.setLevel(Level.FINE);
    }

    /**
     * configure
     * configure the input reader to use an input file.
     */
    @Override
    public void configure() {
        String inputFile = System.getProperty("TxtVehicleRecordParser.inputFile");

        if (inputFile != null) {
            try {
                logger.fine("Read from input file " + inputFile);
                InputStream inputStream = new FileInputStream(inputFile);
                reader = new InputStreamReader(inputStream);
            } catch (Exception e) {
                logger.warning(e.toString());
            }
        } else {
            logger.fine("input file is None, try to read from resources file(for tests).");
            configureUsingResource();
        }
    }

    /**
     * configure
     * configure the input reader to use a resource file.
     */
    public void configureUsingResource() {
        // System.setProperty("TxtVehicleRecordParser.inputFile", "testinput.txt");
        String inputFile = System.getProperty("TxtVehicleRecordParser.inputResourceFile");

        if (inputFile != null) {
            try {
                logger.info("Read from resource file " + this.getClass().getResource("").getPath() + "/" + inputFile);
                InputStream resourceAsStream = this.getClass().getResourceAsStream(inputFile);
                reader = new InputStreamReader(resourceAsStream);
            } catch (Exception e) {
                logger.warning(e.toString());
            }
        }
    }

    /**
     * read lines from the reader.
     * pass the readed lines to parser queue.
     * call parse() to parse states in the parser queue.
     */
    public void read() {
        if (reader == null) {
            logger.severe("reader is null, cannot read");
            return;
        }
        //FileReader fileReader = null;
        BufferedReader br = null;
        Pattern linePattern = Pattern.compile("([a-zA-Z])(\\d+)");

        try {
            //fileReader = new FileReader( file );
            br = new BufferedReader(reader);
            String line = "";
            while ((line = br.readLine()) != null) {
                if (line == null || line.isEmpty()) {
                    continue;
                }

                Matcher matcher = linePattern.matcher(line);
                if (matcher.matches()) {
                    long time = Long.parseLong( matcher.group(2) );
                    char sensor = matcher.group(1).toUpperCase(Locale.getDefault()).charAt(0);
                    if (lastTime > 0 && lastTime > time) {
                        day += 1;
                    }
                    lastTime = time;
                    Mark mark = getVehicleRecordManager().getMark(sensor, time);
                    parseQueue.add(mark);
                    parse();
                }
            }
        } catch (IOException e) {
            logger.warning(e.toString());
        }
    }

    /**
     * parse the state machine.
     * see the impl description in the top directory for detail.
     */
    public void parse() {
        Iterator<Mark> marks = parseQueue.iterator();
        String sensorStr = "";
        Mark mark = null;
        Mark lastMark = null;
        while (marks.hasNext()) {
            mark = marks.next();
            sensorStr += mark.getSensor();
        }
        if (sensorStr.equals("AA")) {
            List<Mark> list = new ArrayList<Mark>(2);
            list.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord =
                getVehicleRecordManager().getVehicleRecord(day, 'N', list, lastNorthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastNorthHit = lastMark.getTime();
        } else if (sensorStr.equals("ABAB")) {
            List<Mark> list = new ArrayList<Mark>(4);
            list.add(parseQueue.remove());
            list.add(parseQueue.remove());
            list.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord = getVehicleRecordManager().getVehicleRecord(day, 'S', list, lastSouthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastSouthHit = lastMark.getTime();
        } else if (sensorStr.equals("AAABAB")) {
            // normally should not come here. But if there is a bad mark, 
            // maybe we will come here after removing the bad mark at the beginning.
            List<Mark> list = new ArrayList<Mark>(2);
            list.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord = getVehicleRecordManager().getVehicleRecord(day, 'N', list, lastNorthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastNorthHit = lastMark.getTime();

            List<Mark> list1 = new ArrayList<Mark>(4);
            list1.add(parseQueue.remove());
            list1.add(parseQueue.remove());
            list1.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list1.add(lastMark);
            VehicleRecord vehicleRecord1 = getVehicleRecordManager().getVehicleRecord(day, 'S', list1, lastSouthHit);
            if (vehicleRecord1 != null) {
                getVehicleRecordManager().add(vehicleRecord1);
            }
            lastSouthHit = lastMark.getTime();
        } else if (sensorStr.equals("ABABAA")) {
            // normally should not come here. But if there is a bad mark,
            // maybe we will come here after removing the bad mark at the beginning.
            List<Mark> list1 = new ArrayList<Mark>(4);
            list1.add(parseQueue.remove());
            list1.add(parseQueue.remove());
            list1.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list1.add(lastMark);
            VehicleRecord vehicleRecord1 = getVehicleRecordManager().getVehicleRecord(day, 'S', list1, lastSouthHit);
            if (vehicleRecord1 != null) {
                getVehicleRecordManager().add(vehicleRecord1);
            }
            lastSouthHit = lastMark.getTime();

            List<Mark> list = new ArrayList<Mark>(2);
            list.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord = getVehicleRecordManager().getVehicleRecord(day, 'N', list, lastNorthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastNorthHit = lastMark.getTime();
        } else if (sensorStr.equals("ABAAAB")) {
            Mark mark1 = parseQueue.remove();
            Mark mark2 = parseQueue.remove();
            List<Mark> list1 = new ArrayList<Mark>(4);
            list1.add(mark1);
            list1.add(mark2);

            List<Mark> list = new ArrayList<Mark>(2);
            list.add(parseQueue.remove()); 
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord = getVehicleRecordManager().getVehicleRecord(day, 'N', list, lastNorthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastNorthHit = lastMark.getTime();

            list1.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list1.add(lastMark);
            VehicleRecord vehicleRecord1 = getVehicleRecordManager().getVehicleRecord(day, 'S', list1, lastSouthHit);
            if (vehicleRecord1 != null) {
                getVehicleRecordManager().add(vehicleRecord1);
            }
            lastSouthHit = lastMark.getTime();
        } else if (sensorStr.equals("ABAABA")) {
            Mark mark1 = parseQueue.remove();
            Mark mark2 = parseQueue.remove();
            Mark mark3 = parseQueue.remove();
            List<Mark> list1 = new ArrayList<Mark>(2);
            list1.add(mark3);

            List<Mark> list = new ArrayList<Mark>(4);
            list.add(mark1);               
            list.add(mark2);
            list.add(parseQueue.remove());
            lastMark = parseQueue.remove();
            list.add(lastMark);
            VehicleRecord vehicleRecord = getVehicleRecordManager().getVehicleRecord(day, 'S', list, lastSouthHit);
            if (vehicleRecord != null) {
                getVehicleRecordManager().add(vehicleRecord);
            }
            lastSouthHit = lastMark.getTime();

            lastMark = parseQueue.remove();
            list1.add(lastMark);
            VehicleRecord vehicleRecord1 = getVehicleRecordManager().getVehicleRecord(day, 'N', list1, lastNorthHit);
            if (vehicleRecord1 != null) {
                getVehicleRecordManager().add(vehicleRecord1);
            }
            lastNorthHit = lastMark.getTime();
        } else if (sensorStr.length() > 6) {
            logger.warning("There is a wrong mark in the inputstream.Try to remove the first mark to re-parse");
            parseQueue.remove();
            parse();
        }
    }
}
