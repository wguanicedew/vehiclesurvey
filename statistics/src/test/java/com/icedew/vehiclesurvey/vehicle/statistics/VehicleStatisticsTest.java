/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

package com.icedew.vehiclesurvey.vehicle.statistics;

import static org.junit.Assert.assertEquals;

import com.icedew.vehiclesurvey.vehicle.statistics.StatisticsReport;
import com.icedew.vehiclesurvey.vehicle.statistics.VehicleStatistics;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.VehicleRecordParserService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.VehicleRecordService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class VehicleStatisticsTest {

    private VehicleRecordManager vehicleRecordManager = null;
    private VehicleRecordService vehicleRecordService = null;
    private VehicleRecordParser vehicleRecordParser = null;
    private VehicleRecordParserService vehicleRecordParserService = null;
    private VehicleStatistics vehicleStatistics = null;
    private long recordSize = 0;

    /**
     * setup before test.
     */
    @Before
    public void setUp() {
        vehicleRecordService = VehicleRecordService.getInstance();
        vehicleRecordManager = vehicleRecordService.getVehicleRecordManager();

        vehicleRecordParserService = VehicleRecordParserService.getInstance();
        vehicleRecordParser = vehicleRecordParserService.getVehicleRecordParser();
        vehicleRecordParser.setVehicleRecordManager(vehicleRecordManager);

        vehicleRecordManager.clear();

        System.setProperty("TxtVehicleRecordParser.inputFile", this.getClass().getResource("testinput.txt").getPath());
        vehicleRecordParser.configure();
        vehicleRecordParser.read();

        recordSize = vehicleRecordManager.getVehicleRecords().size();
        vehicleStatistics = new VehicleStatistics(vehicleRecordManager);
    }

    /**
     * test get vehicles report.
     */
    @Test
    public void getVehiclesReportTest() throws Exception {
        StatisticsReport report = vehicleStatistics.getVehiclesReport(12 * 60);
        // System.out.println(report.toString());
        assertEquals(Boolean.TRUE, (report != null));
        assertEquals(recordSize, report.getTotalCount());
    }

}
