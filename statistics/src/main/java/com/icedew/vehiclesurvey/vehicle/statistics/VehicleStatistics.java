/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/*
 * VehicleStatistics.
 */

package com.icedew.vehiclesurvey.vehicle.statistics;

import com.icedew.vehiclesurvey.vehicle.statistics.StatisticsPeriodReport;
import com.icedew.vehiclesurvey.vehicle.statistics.StatisticsReport;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.VehicleRecordService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


public class VehicleStatistics{

    protected static final Logger logger = Logger.getLogger(VehicleStatistics.class.getName());

    private VehicleRecordManager vehicleRecordManager = null;

    /**
     * constructior.
     */
    public VehicleStatistics(VehicleRecordManager vehicleRecordManager) {
        this.vehicleRecordManager = vehicleRecordManager;
    }

    /**
     * get records with filter.
     */
    public List<VehicleRecord> getVehicles(long day, char direction, long start, long end) {
        List<VehicleRecord> records = vehicleRecordManager.getVehicleRecords(day, direction, start, end);
        return records;
    }

    /**
     * statistics based on a collection of records.
     */
    public void statisticsVehicles(StatisticsPeriodReport report, List<VehicleRecord> records) {
        Map speedMap = new HashMap();
        float totalSpeed = 0;
        float totalDistance = 0;
        long distanceCount = 0;

        speedMap.put("0-30", 0);
        speedMap.put("30-60", 0);
        speedMap.put("60-90", 0);
        speedMap.put("90-", 0);
        for (VehicleRecord r: records) {
            totalSpeed += r.getSpeed();
            if (r.getSpeed() < 30) {
                speedMap.put("0-30", (int)speedMap.get("0-30") + 1);
            } else if (r.getSpeed() < 60) {
                speedMap.put("30-60", (int)speedMap.get("30-60") + 1);
            } else if (r.getSpeed() < 90) {
                speedMap.put("60-90", (int)speedMap.get("60-90") + 1);
            } else {
                speedMap.put("90-", (int)speedMap.get("90-") + 1);
            }

            float distance = r.getDistance();
            if (distance != 0) {
                totalDistance += distance;
                distanceCount += 1;
            }
        }

        long count = records.size();
        float avgDistance = 0;
        if (distanceCount > 0) {
            avgDistance = totalDistance / distanceCount;
        }
        float avgSpeed = totalSpeed / count;
        report.setCount(count);
        report.setAvagSpeed(avgSpeed);
        report.setAvagDistance(avgDistance);
        report.setSpeedMap(speedMap);
    }

    /**
     * generate report with specified step(minutes).
     */
    public StatisticsReport getVehiclesReport(int step) {
        StatisticsReport report = new StatisticsReport(step);
        char[] directions = {'S', 'N'};

        for (int day = 1; day < 6; day += 1) {
            for (char direction: directions) {  
                for (int start = 0; start < 24 * 60; start += step) {
                    StatisticsPeriodReport periodReport
                        = new StatisticsPeriodReport(day, direction, start, start + step);
                    // convert minutes to ms
                    List<VehicleRecord> records
                        = getVehicles(day, direction, start * 60 * 1000, (start + step) * 60 * 1000);
                    statisticsVehicles(periodReport, records);
                    report.add(periodReport);
                }
            }
        }
        report.statistics();
        return report;
    }

}

