/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/*
 * StatisticsPeriodReport
 */

package com.icedew.vehiclesurvey.vehicle.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StatisticsPeriodReport{

    private long day;
    private char direction;
    private long start;
    private long end;
    private long count;
    private Map speedMap;
    private float avgSpeed;
    private float avgDistance;

    /**
     * constructor.
     */
    public StatisticsPeriodReport(long day, char direction, long start, long end) {
        this.day = day;
        this.direction = direction;
        this.start = start;
        this.end = end;
        this.speedMap = null;
        this.avgSpeed = 0;
        this.count = 0;
        this.avgDistance = 0;
    }

    /**
     * set count.
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     * get count.
     */
    public long getCount() {
        return count;
    }

    /**
     * set speed map.
     */
    public void setSpeedMap(Map speedMap) {
        this.speedMap = speedMap;
    }

    /**
     * get speed map.
     */
    public Map getSpeedMap() {
        return speedMap;
    }

    /**
     * set average speed.
     */
    public void setAvagSpeed(float avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    /**
     * get average speed.
     */
    public float getAvagSpeed() {
        return avgSpeed;
    }

    /**
     * set average distance.
     */
    public void setAvagDistance(float avgDistance) {
        this.avgDistance = avgDistance;
    }

    /**
     * get average distance.
     */
    public float getAvagDistance() {
        return avgDistance;
    }

    /**
     * to string.
     */
    public String toString() {
        String str =
            String.format("    *** Day %d ** Direction %s ** Start %4d ** End %4d(minutes from 0 O'Clock) ***\n",
                day, direction, start, end);
        str += String.format("        Total cars: %d\n", count);
        str += String.format("        Average speed: %.2f Km/h\n", avgSpeed);
        str += String.format("        Average distance: %.2f meters\n", avgDistance);
        str += String.format("        Speed distribution:\n");

        for (Object key: speedMap.keySet()) {
            str += String.format("            speed in [%s] number of cars: %d\n", (String)key, speedMap.get(key));
        }

        return str;
    }
}

