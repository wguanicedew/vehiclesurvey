/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/*
 * StatisticsPeriodReport
 */

package com.icedew.vehiclesurvey.vehicle.statistics;

import com.icedew.vehiclesurvey.vehicle.statistics.StatisticsPeriodReport;

import java.util.ArrayList;
import java.util.List;


public class StatisticsReport{

    private List<StatisticsPeriodReport> reports;
    private long step;
    private long totalCount;
    private long avgCountPerHour;
    private long peekCounter;
    private float avgSpeedTotal;
    private float avgDistanceTotal;

    /**
     * constructor.
     */
    public StatisticsReport(long step) {
        this.step = step;
        totalCount = 0;
        avgCountPerHour = 0;
        peekCounter = 0;
        avgSpeedTotal = 0;
        avgDistanceTotal = 0;
        reports = new ArrayList<StatisticsPeriodReport>();
    }

    /**
     * add period report to this.
     */
    public void add(StatisticsPeriodReport report) {
        reports.add(report);
    }

    /**
     * get total count.
     */
    public long getTotalCount() {
        return totalCount;
    }

    /**
     * total statistics from all period reports.
     */
    public void statistics() {
        float totalAvagSpeed = 0;
        float totalAvagDistance = 0;
        for (StatisticsPeriodReport report: reports) {
            totalCount += report.getCount();
            totalAvagSpeed += report.getAvagSpeed();
            totalAvagDistance += report.getAvagDistance();
            if (report.getCount() > peekCounter) {
                peekCounter = report.getCount();
            }
        }
        avgCountPerHour = totalCount / 24;
        avgSpeedTotal = totalAvagSpeed / (reports.size() > 0 ? reports.size() : 1);
        avgDistanceTotal = totalAvagDistance / (reports.size() > 0 ? reports.size() : 1);
    }

    /**
     * to String.
     */
    public String toString() {
        String str = "==================================================================================\n";
        str += "**********************************************************************************\n";
        str += String.format("VehicleSurvey statistics report with period %d minutes\n", step);
        str += String.format("    Total detected cars: %d\n", totalCount);
        str += String.format("    Average detected cars per hour: %d\n", avgCountPerHour);
        str += String.format("    Peek cars number in one period(%d minutes): %d\n", step, peekCounter);
        str += String.format("    Average speed: %.2f Km/h\n", avgSpeedTotal);
        str += String.format("    Average distance between two cars: %.2f meters\n", avgDistanceTotal);

        for (StatisticsPeriodReport report: reports) {
            str += report.toString();
        }

        str += "**********************************************************************************\n";
        str += "==================================================================================\n";

        return str;
    }

    // This part will be useful.
    // public Json toJSON() {}
    
}

