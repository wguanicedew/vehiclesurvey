/* Copyright European Organization for Nuclear Research (CERN)
*
* Licensed under the Apache License, Version 2.0 (the "License");
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
*
* Authors:
* - Wen Guan, <wen.guan@cern.ch>, 2015
*/

/**
* VehicleRecord Service Provider.
*/

package com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VehicleRecordService {
 
    private static final Logger logger = Logger.getLogger(VehicleRecordService.class.getName());

    private static VehicleRecordService service;
    private static ServiceLoader<VehicleRecordManager> loader;
    private static VehicleRecordManager vehicleRecordManager;
 
    private VehicleRecordService() {
        loader = ServiceLoader.load(VehicleRecordManager.class);
    }

    /**
     * static getInstance function.
     */ 
    public static synchronized VehicleRecordService getInstance() {
        if (service == null) {
            service = new VehicleRecordService();
            logger.fine("construct new VehicleRecordService");
        }
        return service;
    }
 
    /**
     * static getVehicleRecordManager function.
     * It will search META-INF/services/'serviceclassname' in the jar file to
     * find the implementation class and then load the class.
     */
    public static VehicleRecordManager getVehicleRecordManager() {

        try {
            getInstance();
            Iterator<VehicleRecordManager> vehicleRecordManagers = loader.iterator();
            if (vehicleRecordManagers.hasNext()) {
                vehicleRecordManager = vehicleRecordManagers.next();
                logger.fine("Found service " + vehicleRecordManager.getClass().getName());
            }
        } catch (ServiceConfigurationError serviceError) {
            logger.warning("Cannot find service " + serviceError.toString());
            vehicleRecordManager = null;
            serviceError.printStackTrace();

        }
        return vehicleRecordManager;
    }
}
