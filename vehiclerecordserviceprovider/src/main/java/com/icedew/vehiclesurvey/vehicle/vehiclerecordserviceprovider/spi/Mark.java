/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 *  Abstract Mark class.
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi;


public abstract class Mark {
    private char sensor;
    private long time;

    /**
     * constructor.
     */ 
    public Mark(char sensor, long time) {
        this.sensor = sensor;
        this.time = time;
    }

    /**
     * set sensor.
     */ 
    public void setSensor(char sensor) {
        this.sensor = sensor;
    }

    /**
     * set time.
     */ 
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * get sensor.
     */ 
    public char getSensor() {
        return sensor;
    }

    /**
     * get time.
     */ 
    public long getTime() {
        return time;
    }

    /**
     * override equals.
     */ 
    public boolean equals(Object obj) {
        if (!(obj instanceof Mark)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Mark mark = (Mark) obj;
        return ((sensor == mark.sensor) && (time == mark.time));
    }

    /**
     * to string.
     */ 
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Mark{");
        sb.append("sensor=").append(sensor);
        sb.append(", timestamp=").append(time);
        sb.append( '}' );
        return sb.toString();
    }
}
