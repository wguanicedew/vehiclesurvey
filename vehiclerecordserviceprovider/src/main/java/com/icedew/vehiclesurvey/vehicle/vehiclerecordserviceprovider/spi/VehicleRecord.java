/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 * Abstract VehicleRecord class.
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;

import java.util.List;

public abstract class VehicleRecord {
    protected long day;
    protected char direction;
    protected float speed;
    protected long lastHit;
    protected float distance;
    protected List<Mark> marks;
    protected static final float axles_distance = (float)2.5;
    protected static final float max_speed = 60;

    /**
     * constructor.
     */ 
    public VehicleRecord(long day, char direction, List<Mark> marks) {
        this.day = day;
        this.direction = direction;
        this.marks = marks;
        lastHit = 0;
        speed = 0;
        distance = 0;
        calSpeedAndDistance();
    }

    /**
     * constructor.
     */ 
    public VehicleRecord(long day, char direction, List<Mark> marks, long lastHit) {
        this.day = day;
        this.direction = direction;
        this.marks = marks;
        this.lastHit = lastHit;
        speed = 0;
        distance = 0;
        calSpeedAndDistance();
    }

    /**
     * set day.
     */ 
    public void setDay(long day) {
        this.day = day;
    }

    /**
     * set direction.
     */ 
    public void setDirection(char direction) {
        this.direction = direction;
    }

    /**
     * set marks.
     */ 
    public void setMarks(List<Mark> marks) {
        this.marks = marks;
        calSpeedAndDistance();
    }

    /**
     * get day.
     */ 
    public long getDay() {
        return day;
    }

    /**
     * get direction.
     */ 
    public char getDirection() {
        return direction;
    }

    /**
     * get marks.
     */ 
    public List<Mark> getMarks() {
        return marks;
    }
    
    /**
     * calculate speed and distance.
     * The speed is calculated by axle distance / (time difference detected on the same sensor).
     * The distance between cars is calculated by the current car's speed * (time difference from last car's hit).
     */ 
    public void calSpeedAndDistance() {
        if (marks.size() == 2) {
            Mark mark1 = marks.get(0);
            Mark mark2 = marks.get(1);
            speed = axles_distance / (mark2.getTime() - mark1.getTime()) * 3600; // return Km/h
            if (lastHit > 0 && lastHit < mark1.getTime()) {
                distance = speed * (mark1.getTime() - lastHit) / 3600;
            }
        } else if (marks.size() == 4) {
            Mark mark1 = marks.get(0);
            Mark mark2 = marks.get(2);
            speed = axles_distance / (mark2.getTime() - mark1.getTime()) * 3600; // return Km/h
            if (lastHit > 0 && lastHit < mark1.getTime()) {
                distance = speed * (mark1.getTime() - lastHit) / 3600;
            }
        }
    }

    /**
     * get speed.
     */ 
    public float getSpeed() {
        return speed;
    }

    /**
     * get distance.
     */ 
    public float getDistance() {
        return distance;
    }

    /**
     * override equals.
     */ 
    public boolean equals(Object obj) {
        if (!(obj instanceof VehicleRecord)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        VehicleRecord vehicleRecord = (VehicleRecord) obj;
        return ((day == vehicleRecord.day) && (direction == vehicleRecord.direction)
            && ((marks == null) ? (vehicleRecord.marks == null) : (marks.equals(vehicleRecord.marks))));
    }

    /**
     * check whether current object matches the given attributes.
     * If yes, return true.
     */ 
    public boolean matchAttribute(long day, char direction, long start, long end) {
        if (!(this.day == day)) {
            return false;
        }
        if (!(this.direction == direction)) {
            return false;
        }
        if (marks.isEmpty()) {
            return false;
        }
        Mark mark = marks.get(0);
        if ((mark.getTime() >= start) && (mark.getTime() < end)) {
            return true;
        }
        return false;
    }
}
