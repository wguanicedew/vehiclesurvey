/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 * Abstract VehicleRecordManager class.
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class VehicleRecordManager {

    protected static final Logger logger = Logger.getLogger(VehicleRecordManager.class.getName());

    /**
     * abstract function.
     * add one record to VehicleRecord manager.
     */ 
    public abstract void add(VehicleRecord vehicleRecord);

    /**
     * abstract function.
     * remove one record from VehicleRecord manager.
     */
    public abstract void remove(VehicleRecord vehicleRecord);

    /**
     * abstract function.
     * clear VehicleRecord manager.
     */ 
    public abstract void clear();

    /**
     * get all records.
     */
    public abstract List<VehicleRecord> getVehicleRecords();

    /**
     * get records with filter.
     */
    public abstract List<VehicleRecord> getVehicleRecords(long day, char direction, long start, long end);

    /**
     * create a new Mark object with given parameters. 
     * Because VehilceRecordManager is loaded dynamically,
     * the Mark class should from the implementation from the same jar file.
     * This function will create a new local implemented Mark object.
     */
    public abstract Mark getMark(char sensor, long time);

    /**
     * create a new VehicleRecord object with given parameters. 
     * Because VehilceRecordManager is loaded dynamically,
     * the VehicleRecord class should from the implementation from the same jar file.
     * This function will create a new local implemented VehicleRecord object.
     */
    public abstract VehicleRecord getVehicleRecord(long day, char direction, List<Mark> marks);

    /**
     * abstract function.
     * Because VehilceRecordManager is loaded dynamically,
     * the VehicleRecord class should from the implementation from the same jar file.
     * This function will create a new local implemented VehicleRecord object.
     */ 
    public abstract VehicleRecord getVehicleRecord(long day, char direction, List<Mark> marks, long lastHit);
}
