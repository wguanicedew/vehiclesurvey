/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider;

import static org.junit.Assert.assertEquals;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.VehicleRecordService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class VehicleRecordServiceTest {

    private static final char sensor = 'A';
    private static final char newSensor = 'B';
    private static final long time = 123;
    private static final long newTime = 1234;
    private static final long day = 1;
    private static final long newDay = 2;
    private static final char direction = 'S';
    private static final char newDirection = 'N';
    private static List<Mark> marks = null;
    private static List<Mark> newMarks = null;

    private Mark mark = null;
    private VehicleRecord vehicleRecord = null;
    private VehicleRecordManager vehicleRecordManager = null;
    private VehicleRecordService service = null;

    /**
     * test get service.
     */ 
    @Test
    public void getServiceTest() throws Exception {
        if (service == null) {
            service = VehicleRecordService.getInstance();
        }
        assertEquals(Boolean.TRUE, (service != null));
    }

    /**
     * test get VehicleRecordManager.
     */ 
    @Test
    public void getVehicleRecordManagerTest() throws Exception {
        getServiceTest();
        if (vehicleRecordManager == null) {
            vehicleRecordManager = service.getVehicleRecordManager();
        }
        assertEquals(Boolean.TRUE, (vehicleRecordManager != null));
    }

    /**
     * test create, set and get mark.
     */ 
    @Test
    public void markTest() throws Exception {
        getVehicleRecordManagerTest();
        mark = vehicleRecordManager.getMark(sensor, time);
        assertEquals(Boolean.TRUE, (mark != null));
        mark.setSensor(newSensor);
        mark.setTime(newTime);
        assertEquals(newSensor, mark.getSensor());
        assertEquals(newTime, mark.getTime());
    }

    /**
     * test create, set and get VehicleRecord.
     */ 
    @Test
    public void vehicleRecordTest() throws Exception {
        getVehicleRecordManagerTest();
        Mark mark1 = vehicleRecordManager.getMark(sensor, time);
        Mark mark2 = vehicleRecordManager.getMark(sensor, time + 1);
        marks = new ArrayList(2);
        marks.add(mark1);
        marks.add(mark2);
        vehicleRecord = vehicleRecordManager.getVehicleRecord(day, direction, marks);
        assertEquals(Boolean.TRUE, (vehicleRecord != null));
        mark1 = vehicleRecordManager.getMark(newSensor, newTime);
        mark2 = vehicleRecordManager.getMark(newSensor, newTime + 1); 
        newMarks = new ArrayList(2);
        newMarks.add(mark1);
        newMarks.add(mark2);
        vehicleRecord.setDay(newDay);
        vehicleRecord.setDirection(newDirection);
        vehicleRecord.setMarks(newMarks);
        assertEquals(newDay, vehicleRecord.getDay());
        assertEquals(newDirection, vehicleRecord.getDirection());
        assertEquals(newMarks, vehicleRecord.getMarks());
    }

    /**
     * test add and remove object to/from VehicleRecordManager.
     */ 
    @Test
    public void vehicleRecordManagerTest() throws Exception {
        getVehicleRecordManagerTest();
        Mark mark1 = vehicleRecordManager.getMark(sensor, time);
        Mark mark2 = vehicleRecordManager.getMark(sensor, time + 1);
        marks = new ArrayList(2);
        marks.add(mark1);
        marks.add(mark2);
        vehicleRecord = vehicleRecordManager.getVehicleRecord(day, direction, marks);
        vehicleRecordManager.add(vehicleRecord);
        List<VehicleRecord> records = vehicleRecordManager.getVehicleRecords();
        assertEquals(records.size(), 1);
        vehicleRecordManager.remove(vehicleRecord);
        assertEquals(records.size(), 0);
    }
}
