/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider;

import static org.junit.Assert.assertEquals;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.VehicleRecordParserService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;

import org.junit.Test;


public class VehicleRecordParserServiceTest {

    /**
     * get servcie test.
     */  
    @Test
    public void getServiceTest() throws Exception {
        VehicleRecordParserService service = VehicleRecordParserService.getInstance();
        assertEquals(Boolean.TRUE, (service != null));
    }

    /**
     * get vehicle record parser test.
     */
    @Test
    public void getVehicleRecordParserTest() throws Exception {
        VehicleRecordParser vehicleRecordParser = VehicleRecordParserService.getVehicleRecordParser();
        assertEquals(Boolean.TRUE, (vehicleRecordParser != null));
    }

}
