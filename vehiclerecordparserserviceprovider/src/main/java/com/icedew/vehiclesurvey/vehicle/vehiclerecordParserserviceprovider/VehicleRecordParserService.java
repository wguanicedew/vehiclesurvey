/*
* Copyright European Organization for Nuclear Research (CERN)
*
* Licensed under the Apache License, Version 2.0 (the "License");
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
*
* Authors:
* - Wen Guan, <wen.guan@cern.ch>, 2015
*/

/**
* VehicleRecordParser Service Provider.
*/

package com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;


public class VehicleRecordParserService {
 
    protected static final Logger logger
        = Logger.getLogger(VehicleRecordParserService.class.getName());

    private static VehicleRecordParserService service;
    private static ServiceLoader<VehicleRecordParser> loader;

    /**
     * constructor.
     */ 
    private VehicleRecordParserService() {
        loader = ServiceLoader.load(VehicleRecordParser.class);
    }
 
    /**
     * get service instance.
     */
    public static synchronized VehicleRecordParserService getInstance() {
        if (service == null) {
            service = new VehicleRecordParserService();
            logger.fine("create new service");
        }
        return service;
    }
 
    /**
     * get vehicle record parser.
     */
    public static VehicleRecordParser getVehicleRecordParser() {
        VehicleRecordParser vehicleRecordParser = null;

        try {
            getInstance();
            Iterator<VehicleRecordParser> vehicleRecordParsers = loader.iterator();
            if (vehicleRecordParsers.hasNext()) {
                vehicleRecordParser = vehicleRecordParsers.next();
                logger.fine("Found new service " + vehicleRecordParser.getClass().getName());
            }
        } catch (ServiceConfigurationError serviceError) {
            logger.warning("Failed to load service" + serviceError.toString());
            vehicleRecordParser = null;
            serviceError.printStackTrace();
        }
        return vehicleRecordParser;
    }
}
