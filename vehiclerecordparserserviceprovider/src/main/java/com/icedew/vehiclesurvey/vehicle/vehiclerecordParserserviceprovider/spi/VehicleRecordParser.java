/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 * Abstract VehicleRecord class.
 */

package com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.io.Reader;
import java.util.List;
import java.util.logging.Logger;


public abstract class VehicleRecordParser {
    protected static final Logger logger = Logger.getLogger(VehicleRecordParser.class.getName());

    protected VehicleRecordManager vehicleRecordManager = null;
    protected Reader reader = null;

    public VehicleRecordParser() {
        this.vehicleRecordManager = null;
    }

    public VehicleRecordParser(VehicleRecordManager vehicleRecordManager) {
        this.vehicleRecordManager = vehicleRecordManager;
    }

    /**
     * configure will setup the reader.
     */
    public void configure() {
    }

    /**
     * configure reader.
     */
    public void configure(Reader reader) {
        this.reader = reader;
    }

    /**
     * set VehicleRecordManager.
     */ 
    public void setVehicleRecordManager(VehicleRecordManager vehicleRecordManager) {
        this.vehicleRecordManager = vehicleRecordManager;
    }

    /**
     * get VehicleRecordManager.
     */ 
    public VehicleRecordManager getVehicleRecordManager() {
        return vehicleRecordManager;
    }

    /**
     * read one line from the reader and add this line to paser stack.
     * Then call parser to parse states in the parse stack.
     */
    public abstract void read();

    /**
     * pass states in the parser stack. If matched state chain founded, return one VehicleRecord.
     */ 
    public abstract void parse();
}
