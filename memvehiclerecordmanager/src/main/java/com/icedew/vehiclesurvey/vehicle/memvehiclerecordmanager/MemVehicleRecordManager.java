/* Copyright European Organization for Nuclear Research (CERN)
*
* Licensed under the Apache License, Version 2.0 (the "License");
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
*
* Authors:
* - Wen Guan, <wen.guan@cern.ch>, 2015
*/

/*
* Memory VehicleRecord Manager.
*/

package com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager;

import com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager.MarkImpl;
import com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager.VehicleRecordImpl;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class MemVehicleRecordManager extends VehicleRecordManager{

    protected static final Logger logger = 
        Logger.getLogger(MemVehicleRecordManager.class.getName());

    private List<VehicleRecord> vehicleRecords = null;

    public MemVehicleRecordManager() {
        vehicleRecords = new ArrayList<VehicleRecord>();
    }

    public void add(VehicleRecord vehicleRecord) {
        vehicleRecords.add(vehicleRecord);
    }

    public void remove(VehicleRecord vehicleRecord) {
        vehicleRecords.remove(vehicleRecord);
    }

    public void clear() {
        vehicleRecords.clear();
    }

    /**
      * get all records.
      */
    public List<VehicleRecord> getVehicleRecords() {
        return vehicleRecords;
    }

    /**
     * get records with filter.
     */
    public List<VehicleRecord> getVehicleRecords(long day, char direction, long start, long end) {
        List<VehicleRecord> retRecords = new ArrayList<VehicleRecord>();
        for (VehicleRecord record: vehicleRecords) {
            if (record.matchAttribute(day, direction, start, end)) {
                retRecords.add(record);
            }
        }
        return retRecords;
    }

    /**
     * create a new Mark object with given parameters. 
     * Because VehilceRecordManager is loaded dynamically, the Mark class should from the 
     * implementation from the same jar file.
     */
    public Mark getMark(char sensor, long time) {
        return new MarkImpl(sensor, time);
    }

    /**
     * create a new VehicleRecord object with given parameters. 
     * Because VehilceRecordManager is loaded dynamically, the VehicleRecord class should from 
     * the implementation from the same jar file.
     */
    public VehicleRecord getVehicleRecord(long day, char direction, List<Mark> marks) {
        return new VehicleRecordImpl(day, direction, marks);
    }

    public VehicleRecord getVehicleRecord(long day, char direction, List<Mark> marks, long lastHit) {
        return new VehicleRecordImpl(day, direction, marks, lastHit);
    }
}
