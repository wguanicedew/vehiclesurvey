/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 *  Abstract Mark class.
 */

package com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager;

import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;

public class MarkImpl extends Mark {
    public MarkImpl(char sensor, long time) {
        super(sensor, time);
    }
}
