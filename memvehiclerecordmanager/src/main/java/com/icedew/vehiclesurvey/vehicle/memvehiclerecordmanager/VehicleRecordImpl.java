/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

/**
 * Abstract VehicleRecord class.
 */

package com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager;

import com.icedew.vehiclesurvey.vehicle.memvehiclerecordmanager.MarkImpl;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.Mark;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecord;

import java.util.List;


public class VehicleRecordImpl extends VehicleRecord {
    public VehicleRecordImpl(long day, char direction, List<Mark> marks) {
        super(day, direction, marks);
    }

    public VehicleRecordImpl(long day, char direction, List<Mark> marks, long lastHit) {
        super(day, direction, marks, lastHit);
    }
}
