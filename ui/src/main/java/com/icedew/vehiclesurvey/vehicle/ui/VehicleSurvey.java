/* Copyright European Organization for Nuclear Research (CERN)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Authors:
 * - Wen Guan, <wen.guan@cern.ch>, 2015
 */

package com.icedew.vehiclesurvey.vehicle.ui;

import com.icedew.vehiclesurvey.vehicle.statistics.StatisticsReport;
import com.icedew.vehiclesurvey.vehicle.statistics.VehicleStatistics;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.VehicleRecordParserService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordparserserviceprovider.spi.VehicleRecordParser;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.VehicleRecordService;
import com.icedew.vehiclesurvey.vehicle.vehiclerecordserviceprovider.spi.VehicleRecordManager;

import java.lang.Integer;
import java.lang.NumberFormatException;
import java.util.ArrayList;
import java.util.List;


public class VehicleSurvey {

    /**
     * main function.
     */
    public static void main(String[] args) {

        VehicleRecordService vehicleRecordService = VehicleRecordService.getInstance();
        VehicleRecordManager vehicleRecordManager = vehicleRecordService.getVehicleRecordManager();

        VehicleRecordParserService vehicleRecordParserService = VehicleRecordParserService.getInstance();
        VehicleRecordParser vehicleRecordParser = vehicleRecordParserService.getVehicleRecordParser();
        vehicleRecordParser.setVehicleRecordManager(vehicleRecordManager);

        vehicleRecordManager.clear();
        // System.setProperty("TxtVehicleRecordParser.inputFile", "testinput.txt");
        vehicleRecordParser.configure();
        vehicleRecordParser.read();

        VehicleStatistics vehicleStatistics = new VehicleStatistics(vehicleRecordManager);

        int step = 0;
        String stepStr = System.getProperty("VehicleSurvey.period");
        if (stepStr == null) {
            System.out.println("VehicleSurvey.period is not defined. return.");
            return;
        }
        try { 
            step = Integer.parseInt(stepStr); 
        } catch (NumberFormatException e) {
            System.out.println(stepStr + " is not integer. return.");
            return;
        }

        StatisticsReport report = vehicleStatistics.getVehiclesReport(step);
        System.out.println(report.toString());

        // define the statistic step by minutes.
        // int[] steps = {12*60, 60, 30, 20, 15};
        // for (int step: steps){
        //     StatisticsReport report = vehicleStatistics.getVehiclesReport(step);
        //     System.out.println(report.toString());
        // }
    }
}
